#include "PRCPoint.h"
#include <random>

PRCPoint::PRCPoint()
{
    randNumb = createRandNumb();
	status = 0;
	label = 1;
}

double PRCPoint::createRandNumb(){
	std::random_device randDev;
	std::mt19937 randGen(randDev());
    std::uniform_real_distribution<float> rule(0.0, 1.0);
	return rule(randGen);
}
