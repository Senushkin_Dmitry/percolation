#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QVector<double> x, y, x1, y1, x2, y2;
    QString strPercent;
    double N;
    N = 0;

    x1.push_back(0);
    x1.push_back(pGrid->getXSize());
    y1.push_back(pGrid->getYSize());
    y1.push_back(pGrid->getYSize());

    x2.push_back(pGrid->getXSize());
    x2.push_back(pGrid->getXSize());
    y2.push_back(0);
    y2.push_back(pGrid->getYSize());

    for(int i = 0; i < pGrid->points.size(); i++){
        for(int j = 0; j < pGrid->points[i].size(); j++){
            if(pGrid->points[i][j]->getStatus() == 1){
                x.push_back(i);
                y.push_back(j);
                N++;
            }
        }
    }
    // create graph and assign data to it:
    ui->widget->addGraph();
    ui->widget->graph(0)->setData(x, y);

    ui->widget->graph(0)->setPen(QColor(50, 50, 50, 255));//задаем цвет точки
    ui->widget->graph(0)->setLineStyle(QCPGraph::lsNone);//убираем линии
    //формируем вид точек
    ui->widget->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

    // give the axes some labels:
    //ui->widget->xAxis->setLabel("x");
    //ui->widget->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    ui->widget->xAxis->setRange(0, pGrid->getXSize()+10);
    ui->widget->yAxis->setRange(0, pGrid->getYSize()+10);

    ui->widget->addGraph();
    ui->widget->graph(1)->setData(x1, y1);
    ui->widget->graph(1)->setPen(QColor(50, 0, 50, 255));

    ui->widget->addGraph();
    ui->widget->graph(2)->setData(x2, y2);
    ui->widget->graph(2)->setPen(QColor(50, 0, 50, 255));

    N = N * 100/ (pGrid->getXSize() * pGrid->getYSize());
    strPercent.setNum(N);
    ui->lineEdit_4->setText(strPercent);

    ui->widget->replot();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->pushButton->setEnabled(true);
    pGrid = new PRCGrid(dimension);

    for(int i = 0; ; i++){
        pGrid->initGrowPoints();
        pGrid->algorithmHoshenKopelman();
        bool t = pGrid->makeStep();
        if(t){
            break;
        }
    }
}

void MainWindow::on_lineEdit_returnPressed()
{
    dimension = ui->lineEdit->text().toInt();
}

void MainWindow::on_pushButton_3_clicked()
{
    QVector<double> LnL, LnL1, LnN, LnN1, vecXDev1, vecYDev1, vecXDev2, vecYDev2, vecXDev3, vecYDev3, vecXDev4, vecYDev4, vecXDev5, vecYDev5;
    QVector<double> xDevUp1, yDevUp1, xDevDown1, yDevDown1, xDevUp2, yDevUp2, xDevDown2, yDevDown2, xDevUp3, yDevUp3, xDevDown3, yDevDown3;
    QVector<double> xDevUp4, yDevUp4, xDevDown4, yDevDown4, xDevUp5, yDevUp5, xDevDown5, yDevDown5, XMNKline, YMNKline;
    QString FracDim;
    double N, L, d, dev1, dev2, dev3, dev4, dev5, mean;
    double xy, squaredx, sx, sy, a;
    N = 0;
    xy = 0;
    squaredx = 0;
    sx = 0;
    sy = 0;

    /*if(pGrid){
        delete pGrid;
    }*/

    for(int k = 0; k < 3; k++){
        L = 10;
        for(int i = 1; i < 6; i++){
            pGrid = new PRCGrid(L);

            for(int i = 0; ; i++){
                pGrid->initGrowPoints();
                pGrid->algorithmHoshenKopelman();
                bool t = pGrid->makeStep();
                if(t){
                    break;
                }
            }

            for(int i = 0; i < pGrid->points.size(); i++){
                for(int j = 0; j < pGrid->points[i].size(); j++){
                    if(pGrid->points[i][j]->getStatus() == 1 && i < L && j < L){
                        N++;
                    }
                }
            }

            N = log(N);
            L = log(L);
            LnN.push_back(N);
            LnL.push_back(L);

            L = (i + 1) * 10;
            N = 0;
            //delete pGrid;
        }
    }

    mean = (LnN[0] + LnN[5] + LnN[10]) / 3;
    dev1 = std::sqrt( (std::pow(LnN[0]- mean, 2) + std::pow(LnN[5]- mean, 2) + std::pow(LnN[10]- mean, 2)) / 6 );
    mean = (LnN[1] + LnN[6] + LnN[11]) / 3;
    dev2 = std::sqrt( (std::pow(LnN[1]- mean, 2) + std::pow(LnN[6]- mean, 2) + std::pow(LnN[11]- mean, 2)) / 6 );
    mean = (LnN[2] + LnN[7] + LnN[12]) / 3;
    dev3 = std::sqrt( (std::pow(LnN[2]- mean, 2) + std::pow(LnN[7]- mean, 2) + std::pow(LnN[12]- mean, 2)) / 6 );
    mean = (LnN[3] + LnN[8] + LnN[13]) / 3;
    dev4 = std::sqrt( (std::pow(LnN[3]- mean, 2) + std::pow(LnN[8]- mean, 2) + std::pow(LnN[13]- mean, 2)) / 6 );
    mean = (LnN[4] + LnN[9] + LnN[14]) / 3;
    dev5 = std::sqrt( (std::pow(LnN[4]- mean, 2) + std::pow(LnN[9]- mean, 2) + std::pow(LnN[14]- mean, 2)) / 6 );

    vecXDev1.push_back(LnL[0]);
    vecXDev1.push_back(LnL[0]);
    vecYDev1.push_back(LnN[0] + dev1);
    vecYDev1.push_back(LnN[0] - dev1);
    xDevUp1.push_back(LnL[0] - 0.01);
    xDevUp1.push_back(LnL[0] + 0.01);
    yDevUp1.push_back(LnN[0] + dev1);
    yDevUp1.push_back(LnN[0] + dev1);
    xDevDown1.push_back(LnL[0] - 0.01);
    xDevDown1.push_back(LnL[0] + 0.01);
    yDevDown1.push_back(LnN[0] - dev1);
    yDevDown1.push_back(LnN[0] - dev1);

    vecXDev2.push_back(LnL[1]);
    vecXDev2.push_back(LnL[1]);
    vecYDev2.push_back(LnN[1] + dev2);
    vecYDev2.push_back(LnN[1] - dev2);
    xDevUp2.push_back(LnL[1] - 0.01);
    xDevUp2.push_back(LnL[1] + 0.01);
    yDevUp2.push_back(LnN[1] + dev2);
    yDevUp2.push_back(LnN[1] + dev2);
    xDevDown2.push_back(LnL[1] - 0.01);
    xDevDown2.push_back(LnL[1] + 0.01);
    yDevDown2.push_back(LnN[1] - dev2);
    yDevDown2.push_back(LnN[1] - dev2);

    vecXDev3.push_back(LnL[2]);
    vecXDev3.push_back(LnL[2]);
    vecYDev3.push_back(LnN[2] + dev3);
    vecYDev3.push_back(LnN[2] - dev3);
    xDevUp3.push_back(LnL[2] - 0.01);
    xDevUp3.push_back(LnL[2] + 0.01);
    yDevUp3.push_back(LnN[2] + dev3);
    yDevUp3.push_back(LnN[2] + dev3);
    xDevDown3.push_back(LnL[2] - 0.01);
    xDevDown3.push_back(LnL[2] + 0.01);
    yDevDown3.push_back(LnN[2] - dev3);
    yDevDown3.push_back(LnN[2] - dev3);

    vecXDev4.push_back(LnL[3]);
    vecXDev4.push_back(LnL[3]);
    vecYDev4.push_back(LnN[3] + dev4);
    vecYDev4.push_back(LnN[3] - dev4);
    xDevUp4.push_back(LnL[3] - 0.01);
    xDevUp4.push_back(LnL[3] + 0.01);
    yDevUp4.push_back(LnN[3] + dev4);
    yDevUp4.push_back(LnN[3] + dev4);
    xDevDown4.push_back(LnL[3] - 0.01);
    xDevDown4.push_back(LnL[3] + 0.01);
    yDevDown4.push_back(LnN[3] - dev4);
    yDevDown4.push_back(LnN[3] - dev4);

    vecXDev5.push_back(LnL[4]);
    vecXDev5.push_back(LnL[4]);
    vecYDev5.push_back(LnN[4] + dev5);
    vecYDev5.push_back(LnN[4] - dev5);
    xDevUp5.push_back(LnL[4] - 0.01);
    xDevUp5.push_back(LnL[4] + 0.01);
    yDevUp5.push_back(LnN[4] + dev5);
    yDevUp5.push_back(LnN[4] + dev5);
    xDevDown5.push_back(LnL[4] - 0.01);
    xDevDown5.push_back(LnL[4] + 0.01);
    yDevDown5.push_back(LnN[4] - dev5);
    yDevDown5.push_back(LnN[4] - dev5);

    for(int i = 0; i < 5; i++){
        LnL1.push_back(LnL[i]);
        LnN1.push_back(LnN[i]);
    }

    // create graph and assign data to it:
    ui->widget_2->addGraph();
    ui->widget_2->graph(0)->setData(LnL1, LnN1);

    ui->widget_2->graph(0)->setPen(QColor(50, 50, 50, 255));//задаем цвет точки
    //ui->widget->graph(0)->setLineStyle(QCPGraph::lsNone);//убираем линии
    //формируем вид точек
    ui->widget_2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

    // give the axes some labels:
    ui->widget_2->xAxis->setLabel("Ln(L)");
    ui->widget_2->yAxis->setLabel("Ln(N)");
    // set axes ranges, so we see all data:
    ui->widget_2->xAxis->setRange(2, 4);
    ui->widget_2->yAxis->setRange(3, 8);

    ui->widget_2->addGraph();
    ui->widget_2->graph(1)->setData(vecXDev1, vecYDev1);

    ui->widget_2->addGraph();
    ui->widget_2->graph(2)->setData(vecXDev2, vecYDev2);

    ui->widget_2->addGraph();
    ui->widget_2->graph(3)->setData(vecXDev3, vecYDev3);

    ui->widget_2->addGraph();
    ui->widget_2->graph(4)->setData(vecXDev4, vecYDev4);

    ui->widget_2->addGraph();
    ui->widget_2->graph(5)->setData(vecXDev5, vecYDev5);

    ui->widget_2->addGraph();
    ui->widget_2->graph(6)->setData(xDevUp1, yDevUp1);

    ui->widget_2->addGraph();
    ui->widget_2->graph(7)->setData(xDevUp2, yDevUp2);

    ui->widget_2->addGraph();
    ui->widget_2->graph(8)->setData(xDevUp3, yDevUp3);

    ui->widget_2->addGraph();
    ui->widget_2->graph(9)->setData(xDevUp4, yDevUp4);

    ui->widget_2->addGraph();
    ui->widget_2->graph(10)->setData(xDevUp5, yDevUp5);

    ui->widget_2->addGraph();
    ui->widget_2->graph(11)->setData(xDevDown1, yDevDown1);

    ui->widget_2->addGraph();
    ui->widget_2->graph(12)->setData(xDevDown2, yDevDown2);

    ui->widget_2->addGraph();
    ui->widget_2->graph(13)->setData(xDevDown3, yDevDown3);

    ui->widget_2->addGraph();
    ui->widget_2->graph(14)->setData(xDevDown4, yDevDown4);

    ui->widget_2->addGraph();
    ui->widget_2->graph(15)->setData(xDevDown5, yDevDown5);

    for(int i = 0; i < 5; i++){
        sx += LnL[i];
        sy += LnN[i];
        xy += LnN[i] * LnL[i];
        squaredx += LnL[i] * LnL[i];
    }
    d = (sx * sy - 5 * xy) / (sx * sx - 5 * squaredx);
    a = (sx * xy - squaredx * sy)/(sx * sx - 5 * squaredx);

    XMNKline.push_back(0);
    XMNKline.push_back(4);
    YMNKline.push_back(a);
    YMNKline.push_back(4 * d + a);

    ui->widget_2->addGraph();
    ui->widget_2->graph(16)->setData(XMNKline, YMNKline);
    ui->widget_2->graph(0)->setPen(QColor(255, 0, 0, 255));
    ui->widget_2->replot();

    FracDim.setNum(d);
    ui->lineEdit_2->setText(FracDim);
    //a = exp(a);
    FracDim.setNum(a);
    ui->lineEdit_3->setText(FracDim);
}
