#-------------------------------------------------
#
# Project created by QtCreator 2015-05-24T13:07:44
#
#-------------------------------------------------

QT       += core gui widgets
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport


TARGET = testform2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    PRCGrid.cpp \
    PRCPoint.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    PRCPoint.h \
    PRCGrid.h

FORMS    += mainwindow.ui
