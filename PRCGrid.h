#ifndef PRCGRID_H
#define PRCGRID_H
#include <vector>
#include <fstream>
#include"PRCPoint.h"
#include <QVector>

class PRCGrid
{
public:
    PRCGrid(int L = 10);

    std::vector< std::vector<PRCPoint*> > points;
    std::vector<PRCPoint*> growPoints;

    double getXSize(){ return xSize; }
    double getYSize(){ return ySize; }

    void algorithmHoshenKopelman();
    void initGrowPoints();
    bool makeStep();
    void pushBackGrowPoint(PRCPoint* point);
    void initPointsCoordinates();
    void removeTrappedClustersLabel(int gLabel);
    void printMatrix(std::fstream& fileOut);
    //QVector<*PRCPoint> copyQVector();

    			int uf_find(int x);
                int uf_union(int x, int y);
                int uf_make_set();
                void uf_initialize(int max_labels);
                void uf_done();
                int hoshen_kopelman(std::vector< std::vector<int> > &matrix, int m, int n);

private:
	int xSize;
	int ySize;
	int gLabel;
};

#endif // PRCGRID_H
