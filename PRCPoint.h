#ifndef PRCPOINT_H
#define PRCPOINT_H
#include<cmath>

class PRCPoint
{
public:
    PRCPoint();

    void setX(int x1){ x = x1; }
    void setY(int y1){ y = y1; }
    void setStatus(int stat){ status = stat; }
    void setLabel(int lab){ label = lab; }
    void setRandNumb(double num){ randNumb = num; }

    int getX(){ return x; }
    int getY(){ return y; }
    int getStatus(){ return status; }
    int getLabel(){ return label; }
    double getRandNumb(){ return randNumb; }

    static double createRandNumb();
    void initCoordinates(int x, int y);

    PRCPoint& operator=(PRCPoint tmp);
    bool operator==(PRCPoint tmp);
private:
	double randNumb;
	int label;
	int status;
	int x;
	int y;


};

#endif // PRCPOINT_H
