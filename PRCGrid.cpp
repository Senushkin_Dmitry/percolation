#include "PRCGrid.h"
#include <assert.h>
#include <malloc.h>

PRCGrid::PRCGrid(int L){

    xSize = 3 * L;
    ySize = 2 * L;
	points.resize(xSize);
    growPoints.resize(0);

    for(int i = 0; i < xSize; i++)
		points[i].resize(ySize);

	for(int i = 0; i < xSize; i++){
		for(int j = 0; j < ySize; j++){
			points[i][j] = new PRCPoint;
		}
	}
    for(int i = 0; i<L; i++){
        points[0][i]->setStatus(1);
        points[0][i]->setLabel(0);
	}
	initPointsCoordinates();
}

void PRCGrid::pushBackGrowPoint(PRCPoint* point){
    int cnt = 0;
    /*for(std::vector<PRCPoint*>::iterator i = growPoints.begin(); i != growPoints.end(); i++){
        PRCPoint* test;
        test = *i;*/
    if(!growPoints.empty())
        for(unsigned int i = 0; i < growPoints.size(); i++){
            cnt = (growPoints[i] == point) ? (cnt+1) : cnt;
        }
	if(!cnt){
		growPoints.push_back(point);
	}
}

void PRCGrid::initPointsCoordinates(){
    for(int i = 0; i < xSize; i++){
        for(int j = 0; j < ySize; j++){
			points[i][j]->setX(i);
			points[i][j]->setY(j);
		}
	}
}

void PRCGrid::initGrowPoints(){
    growPoints.clear();

    for(int i = 0; i<(xSize-1); i++){
        for(int j = 0; j<ySize; j++){
            if((points[i][j]->getStatus() == 1)){
                if((j>0)&&(j<ySize-1)){
                    if(points[i][j-1]->getStatus() == 0){
                        pushBackGrowPoint(points[i][j-1]);
                    }
                    if(points[i][j+1]->getStatus() == 0){
                        pushBackGrowPoint(points[i][j+1]);
                    }
                }
                else{
                    if(j==0){
                        if(points[i][j+1]->getStatus() == 0){
                            pushBackGrowPoint(points[i][j+1]);
                        }
                    }
                    else{
                        if(j==(ySize-1))
                            if(points[i][j-1]->getStatus() == 0){
                                pushBackGrowPoint(points[i][j-1]);
                            }
                        }
                }
                if((i>0)&&(i<(xSize-1))){
                    if(points[i+1][j]->getStatus() == 0){
                        pushBackGrowPoint(points[i+1][j]);
                    }
                    if(points[i-1][j]->getStatus() == 0){
                        pushBackGrowPoint(points[i-1][j]);
                    }
                    /*if((j>0)&&(j<ySize-1)){
                        if((allNode[i+1][j+1].status!=0)||(allNode[i+1][j-1].status!=0)||
                           (allNode[i-1][j-1].status!=0)||(allNode[i-1][j+1].status!=0)){
                            vGrowNode.push_back(allNode[i][j]);
                        }*/
                    }
                else{
                    if(i==0)
                        if(points[i+1][j]->getStatus() == 0){
                            pushBackGrowPoint(points[i+1][j]);
                        }
                }    
            }
        }
    }
}

void PRCGrid::algorithmHoshenKopelman(){
	std::vector< std::vector<int> > vTmp(xSize);

    for(int i = 0; i < xSize; i++){
		vTmp[i].resize(ySize);
	}
    for(int i = 0; i < xSize; i++){
        for(int j = 0; j < ySize; j++){
			vTmp[i][j] = points[i][j]->getLabel();
		}
	}

    gLabel = hoshen_kopelman(vTmp, xSize, ySize);

    for(int i = 0; i < xSize; i++){
        for(int j = 0; j < ySize; j++){
			points[i][j]->setLabel(vTmp[i][j]);
		}
	}

    removeTrappedClustersLabel(gLabel++);
}

void PRCGrid::removeTrappedClustersLabel(int gLabel){
    std::vector<PRCPoint*> vec;
    for(std::vector<PRCPoint*>::iterator i = growPoints.begin(); i != growPoints.end(); i++){
        PRCPoint* test;
        test = *i;
        if(test->getLabel() == gLabel){
            vec.push_back(*i);
		}
	}
    growPoints.clear();
    for(std::vector<PRCPoint*>::iterator i = vec.begin(); i != vec.end(); i++){
        growPoints.push_back(*i);
    }
}

bool PRCGrid::makeStep(){
    PRCPoint* stepPoint;
	double min;

    min = 1;

	for(std::vector<PRCPoint*>::iterator i = growPoints.begin(); i != growPoints.end(); i++){
        PRCPoint* test;
        test = *i;
        if(test->getRandNumb() < min){
            min = test->getRandNumb();
			stepPoint = *i;
		}
	}

    int a = stepPoint->getX();
    int b = stepPoint->getY();
    points[a][b]->setLabel(0);
    points[a][b]->setStatus(1);

	if(stepPoint->getX() == xSize-1)
		return true;
	else
		return false;
}

int *labels;
int  n_labels = 0;

int PRCGrid::uf_find(int x)
{
      int y = x;
      while (labels[y] != y)
            y = labels[y];

      while (labels[x] != x)
      {
            int z = labels[x];
            labels[x] = y;
            x = z;
      }
      return y;
}

int PRCGrid::uf_union(int x, int y)
{
    return labels[uf_find(x)] = uf_find(y);
}

int PRCGrid::uf_make_set()
{
      labels[0] ++;
      assert(labels[0] < n_labels);
      labels[labels[0]] = labels[0];
      return labels[0];
}

void PRCGrid::uf_initialize(int max_labels)
{
      n_labels = max_labels;
      labels = new int[n_labels];
      labels[0] = 0;
}

void PRCGrid::uf_done()
{
      n_labels = 0;
      free(labels);
      labels = 0;
}

int PRCGrid::hoshen_kopelman(std::vector< std::vector<int> > &matrix, int m, int n)
{

      uf_initialize(m * n / 2);

      for (int i=0; i<m; i++)
           for (int j=0; j<n; j++)
                if (matrix[i][j])
                {

                    int up = (i==0 ? 0 : matrix[i-1][j]);    //  проверяем сверху
                    int left = (j==0 ? 0 : matrix[i][j-1]);  //  проверяем слева

                    switch (!!up + !!left)
                    {
                    case 0:
                        matrix[i][j] = uf_make_set();       //  новый кластер
                        break;

                    case 1:                                 // часть существующего кластера
                        matrix[i][j] = std::max(up,left);
                        break;

                    case 2:                                 // ячейка граничит с 2 кластерами
                        matrix[i][j] = uf_union(up, left);
                        break;
                    }
              }

      int *new_labels = new int[n_labels];

      for (int i=0; i<m; i++)
            for (int j=0; j<n; j++)
                if (matrix[i][j])
                {
                    int x = uf_find(matrix[i][j]);
                    if (new_labels[x] == 0)
                    {
                        new_labels[0]++;
                        new_labels[x] = new_labels[0];
                    }
                    matrix[i][j] = new_labels[x];
                }

      //int total_clusters = new_labels[0];

      int total_clusters = matrix[xSize-1][ySize-1];

      free(new_labels);
      uf_done();

      return total_clusters;
}

void PRCGrid::printMatrix(std::fstream& fileOut){
	for(int i = 0; i < xSize; i++){
		fileOut<<"\n";
		for(int j = 0; j < ySize; j++){
			fileOut<<points[i][j]->getLabel()<<" ";
		}
	}
	/*for(u_int j = 0; j<growPoints.size(); j++)
        {
            fileOut<<growPoints[j]->getX()<<"\t"<<growPoints[j]->getY()<<"\t"<<growPoints[j]->getStatus()<<"\t"<<growPoints[j]->getLabel()<<"\n";
        }*/
}

/*QVector<*PRCPoint> PRCGrid::copyQVector(){
    QVector<*PRCPoint> newQvec;
    for(int i = 0; i < points.size(); i++){
        for(int j = 0; j < points[i].size(); j++){
            newQvec.push_back(points[i][j]);
        }
    }
    return newQvec;
}*/
